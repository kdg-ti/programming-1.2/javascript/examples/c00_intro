// calback functions: function as parameter
function square(x) {
    return x * x;
}

const cube =function(x){
    return x*x*x;
}

function doAndReport(task,value){
    console.log("Job done with result: ",task(value))
}

doAndReport(square,3);
doAndReport(cube,4);
doAndReport(a => a*5,2);
doAndReport( () => cube(5));




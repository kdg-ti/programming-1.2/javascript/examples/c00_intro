let bottle
console.log(bottle) // undefined
console.log(bottle??"bottle is empty") // bottle is empty
bottle="Orval"
console.log(bottle??"bottle is empty") // Orval

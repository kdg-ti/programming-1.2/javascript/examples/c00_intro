// IntelliJ supports JSDoc out of the box: https://jsdoc.app/
// Syntax is similar to JavaDoc syntax.

/**
 * Doubles any given number.
 * @param {number} n Just a number.
 * @returns {number} Twice the given number.
 */
function timesTwo(n) {
    return n * 2;
}

/**
 * The user's age.
 * @type {number}
 */
/**
 * The user's age
 * Adding type information makes sense here since we're not
 * initializing the variable at declaration.
 * @type {number} age
 */
let age;
age = '20'; // <-- Warning!

//More example code

// (number) -> number
function calculateSquare(a) {
    return a * a; // Pressing CTRL-Q on the function's name won't reveal type information.
}

// We can even use JSDoc to annotate variables, although often
// IDEs can derive type information from initializers.

/**
 * Adding type information to a const doesn't provide any benefits,
 * and, as a result, is discouraged. (the IDE can derive the type)
 * @type {string} lastName
 */
const lastName = 'Mercury';

/**
 * Adding type information makes sense here since 'name' is
 * not a 'const', and we want to prevent its type from changing.
 * @type {string} name
 */
let name = 'Freddy';
name = 7; // <-- Warning!


// closures
let introduction = "Hello"
function greet(name){
    console.log(`${introduction} ${name}!`)
    introduction+=` ${name} and`
}
greet("Sebastiaan");
greet("Christian");
introduction="Goodbye"
greet("Jan");
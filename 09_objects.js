let day1 = {
    holiday: false,
    "to do": "work"
};
console.log(day1.holiday); // false
console.log(day1["to do"]); // work
console.log(day1.lunch); // undefined

day1.lunch = "pizza";
console.log(day1.lunch); // pizza

// computed property
let meal = "breakfast";
day1[meal]="muesli";
console.log(day1.breakfast); // muesli


day1.lunch = {
    main:"mussels",
    desert:"rice pudding"
};
console.log(day1);
// {
//     holiday: false,
//     'to do': 'work',
//     lunch: { main: 'mussels', desert: 'rice pudding' }
//     breakfast: 'muesli'
// }

console.log(Object.keys(day1)); // [ 'holiday', 'to do', 'lunch','breakfast' ]
if(! ("starter" in day1.lunch)){
    day1.lunch.starter="tomate crevette"
}

for (let food in day1.lunch){
    console.log(food); // main, desert, starter
}
// object methods
shop = {
    drink:"Guinnes",
    print(){
        console.log(day1.breakfast,day1.lunch,this.drink);
    }
}

shop.print();
// muesli { main: 'mussels', desert: 'rice pudding', starter: 'tomate crevette' } Guinnes

lambdaShop = {
    drink:"Guinnes",
    print: () => console.log(day1.breakfast,day1.lunch,this.drink)
}
lambdaShop.print();

//optional chaining
console.log(day1.secondBreakfast);
console.log(day1.secondBreakfast.drink);
if(day1.secondBreakfast && day1.secondBreakfast.drink){
    console.log(day1.secondBreakfast.drink)
} else{
    console.log ("coffee")
}
console.log(day1.secondBreakfast?.drink ?? "coffee");